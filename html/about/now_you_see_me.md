# Now You See Me

 

> Using a non-native language can be less embarrassing yourself when you try to express something emotional and hide it from people who don’t even care, and that’s why I decided to write this in English.

 

## What I’m feeling now

 

Been feeling depressed these couple of days and I tried, so many times, telling myself that it’s ok, don’t be, and I just couldn’t control my mind though I’m fully aware that I get finals to prepare. Sleep was deprived, mind was distracted.

 

Days ago, I knew that day would be the happiest or the darkest day of my monotonous life as it had been. And it turns out the latter one stood out as it always did.

 

Then I kept telling myself that the result was what you wanted for years as you always claimed, and you lost nothing. But was that just a lie you used to comfort yourself? I don’t know and I can’t figure that out. The part of me that is wise and reasonable seemed to lose his fight, and the other side of me which had been oppressed for so long finally stood out and that made me scared. 

 

## So I wrote this down

 

I tried to find answers, in books, in philosophy, in talks. I tried to exercise, with friends talking some everyday stuff. Then I got tired and slowly fell asleep. But the moment I woke up these feeling start to haunt me. I need to focus, so I wrote this down, for nothing but trying to express something hoping that this would help me to let go of it.

 

## Answers I’ve been searching for

 

What is the question? Oh, there are bunches of them. What is the one that you’re trying to answer? Oh good, now there’s another question.

 

I used to believe in rational choice theory and that well-educated people’s behavior is subject to it. So I observe everything a person choose to do and try to find a reasonable explanation for why he/she is doing so and usually I get one. Exceptions are explained by some assumptions I make up and so the theory works just fine. But now I’m standing right in the middle of exceptions and cannot figure out why.

 

I had been working so hard for almost no specific motivation and even no aim that is good for myself. I would do what I was told was right to do by society and by education. Remarkable endurance was required, as well as self-discipline, people liked me and complimented me and saw me as an extraordinary kid, however, sacrifice was made to achieve all this, and that’s me hiding myself from literally everyone.

 

## Then I started to discover myself

 

I didn’t know how long I should hide and kept pretending that some part of me never existed, until I broke down which seems not that bad now because it forced me to discover myself and, perhaps, answer the question: what am I doing all these for?

 

I once thought all these ultimate questions are hilarious since it is not like some difficult math problem but some question that everyone can answer, like money, like a bright future. But now I find myself rather solving a difficult math problem than thinking of a question that at my age almost no one can give answer to.

 

The road to finding the answer exhausted every piece of me. I lied on the bed, thinking about my past and my future and couldn’t sleep. So many times did I wake up in the middle of the night feeling desperate and scared, of the unknown. 

 

But it also witnessed the part of me that had long been hiding walked out of shadow and standing in the light.

 

**I tell my beloved ones how I really feel. I reject people by telling them the real reason I do so. I give my friends a strong hug when parting is inevitable. I cry when beautiful things really touch me. I dance with confidence in front of dozens of people. I give a speech like it is really my intent to say so.**

 

I cut myself wide open. Too straightforward as one may consider, I show people my real self hoping that they could do the same, which is something that really takes courage to do.

 

## And now I see myself

 

It is truly a bliss that I didn’t lost myself but just hid it and now I find it back.

 

I find myself highly sensitive, being able to feel tiny emotional vibration. It bothers me so deeply that I could feel pains like they’re right in front of my eyes and I couldn’t hold the excitement and happiness like they’re drugs making me keeping searching for them.

 

I find myself lack the patience of waiting. I see my goal and then running without reservation to grab it. And the waiting one must endure makes me painful.

 

I find myself being not only gifted in computer science but in social science and art. So I decide to reach out to them, to learn how the society functions and how to do beautiful dance moves.

 

I find myself lack the ability to express. So I begin to make new friends and chat with old ones more frequently.

 

I find myself not that ambitious. No one can say that they does not want to achieve something big, but I’d rather be an ordinary people pursuing the ordinary happiness.

 

I find myself lack the ability to love, to give. So I try to be considerate and to love.

 

Whatever I found about myself, I show all of them to people around. No more hiding. If it’s something good, I’d show you with confidence that I never had and if it’s not, I’ll show you with honesty.

 

**And now you see me.**

 

2018-07-04

 

> I am REALLY appreciated that you could finish reading this and that means A LOT to me. I’m sorry if some negative feeling I wrote bothered you and I would be more than happy if something I wrote inspired you.